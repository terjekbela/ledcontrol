////////////////////////////////////////////////////////////////////////////////
// LEDControl
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
// Libraries used
////////////////////////////////////////////////////////////////////////////////

#define LEDCONTROL_VER "v1.0.21"           // version number

#include <Adafruit_NeoPixel.h>             // Adafruit NeoPixel lib

//#include <SPI.h>                         // Arduino standard Ethernet/SPI lib
//#include <Ethernet.h>                      

#include <UIPEthernet.h>                   // UIP Ethernet library



////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// global configuration
const byte ledMode PROGMEM       = 3;      // 1=directIO; 2=shiftReg; 3=neoPixel
const byte ledCount PROGMEM      = 150;    // number of leds

// direct io pin configuration
const byte dioPins[] PROGMEM     = {       // io pins used in the right sequence
  1,0,2,3,4,5,6,7,8,9,10,11,
  12,19,18,17,16,15,14,13
};

// shift register configuration
const byte shiftRegData PROGMEM  = 4;      // first shift registers data pin
const byte shiftRegLatch PROGMEM = 3;      // first shift registers latch pin
const byte shiftRegClock PROGMEM = 2;      // first shift registers clock pin
const byte shiftRegBits PROGMEM  = 8;      // number of bit is a shif register
const byte shiftRegCount PROGMEM = 5;      // number of shift registers

// neopixel configuration
const byte neoPin PROGMEM        = 9;      // data pin of the strip
const byte neoRed PROGMEM        = 255;    // rgb color correction - red
const byte neoGreen PROGMEM      = 160;    // rgb color correction - green
const byte neoBlue PROGMEM       = 40;     // rgb color correction - blue

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xBE,
  0xEF, 0xFE, 0xED
};
byte netIP[]                     = {       // ip address
  192, 168, 15, 18
};



////////////////////////////////////////////////////////////////////////////////
// Runtime variables
////////////////////////////////////////////////////////////////////////////////

// led automation
volatile byte ledSequenceNo = 0;           // actual sequence running
volatile long ledSpeed      = 100;         // led stepper speed in millisecs
volatile byte ledBrightness = 16;          // max brightness
volatile long ledOrigMillis = 0;           // multi-purpose registers

// adafruit strip
Adafruit_NeoPixel strip = 
  Adafruit_NeoPixel(ledCount, neoPin, NEO_GRB + NEO_KHZ800);
  
//eth
EthernetServer netServer(80);



////////////////////////////////////////////////////////////////////////////////
// Setup / main section
////////////////////////////////////////////////////////////////////////////////

void setup() {
  switch(ledMode) {
  case 1:
    for(byte i=0; i<ledCount; i++){
      pinMode(dioPins[i], OUTPUT);
    }
    break;
  case 2:
    pinMode(shiftRegData,  OUTPUT);
    pinMode(shiftRegLatch, OUTPUT);
    pinMode(shiftRegClock, OUTPUT);
    break;
  case 3:
    strip.begin();
    strip.show();
    break;
  }
  Ethernet.begin(netMAC, netIP);
  netServer.begin();
}



void loop() {
  interruptLed();
//  interruptSerial();
  interruptHttp();
  delay(ledSpeed / 10);
}



////////////////////////////////////////////////////////////////////////////////
// Interrupt and timer handlers
////////////////////////////////////////////////////////////////////////////////

// interrupt handler for led animation
void interruptLed() {
  switch(ledMode) {
  case 1: 
    ledDisplayDio();   
    break;
  case 2: 
    ledDisplayShift(); 
    break;
  case 3: 
    ledDisplayNeo();   
    break;
  }
}

// interrupt handler for serial i/o
void interruptSerial() {
//
}

void interruptHttp() {
  boolean currentBlank;
  String currentLine;
  String currentURL;
  char c;
  EthernetClient client = netServer.available();
  if (client) {
    currentBlank = true;
    currentLine  = "";
    currentURL   = "";
    while (client.connected()) {
      //interruptLed();
      if (client.available()) {
        c = client.read();
        if (c == '\n' && currentBlank) {
          httpProcess(client, currentURL);
          break;
        }
        if (c == '\n') {
          currentBlank = true;
          if(currentLine.indexOf('GET') > 0) {
            currentURL = currentLine;
            currentURL.replace("GET ", "");
            currentURL.replace(" HTTP/1.1", "");
            currentLine = "";
          }
        } else if (c != '\r') {
          currentBlank = false;
          currentLine.concat(c);
        }
      }
    }
    delay(10);
    client.stop();
  }
}



////////////////////////////////////////////////////////////////////////////////
// LED display functions
////////////////////////////////////////////////////////////////////////////////

// displays led status with simple io pins
void ledDisplayDio() {
  for(byte i=0; i<ledCount; i++){
    if(ledGetSequence(ledSequenceNo, millis() - ledOrigMillis, i) > 0) {
      digitalWrite(dioPins[i], 1);
    } else {
      digitalWrite(dioPins[i], 0);
    }
  }
}

// displays led status with the shift registers
void ledDisplayShift() {
  volatile uint32_t ledStatus[]   = {
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0
  };
  byte reg = 0;
  byte cnt = 0;
  for(byte i=0; i<ledCount; i++){
    ledStatus[i] = ledGetSequence(ledSequenceNo, millis() - ledOrigMillis, i);
  }
  digitalWrite(shiftRegLatch,LOW);
  if(ledCount < (shiftRegCount * shiftRegBits)) {
    cnt += (shiftRegCount * shiftRegBits - ledCount);
  }
  for(byte i=ledCount; i>0; i--) {
    reg = reg * 2;
    if(ledStatus[i - 1] > 0) reg++;
    cnt++;
    if(cnt == shiftRegBits) {
      shiftOut(shiftRegData, shiftRegClock, MSBFIRST, reg);
      reg=0;
      cnt=0;
    }
  }
  digitalWrite(shiftRegLatch,HIGH);
}

// displays led status on neopixel WS2411/WS2412 rgb leds
void ledDisplayNeo() {
  byte  r,g,b;
  uint32_t c;
//  volatile uint32_t ledStatus[]   = {
//    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
//  };
//  for(byte i=0; i<ledCount; i++){
//    ledStatus[i] = ledGetSequence(ledSequenceNo, millis() - ledOrigMillis, i);
//  }
  for(byte i=0; i<ledCount; i++){
    c = ledGetSequence(ledSequenceNo, millis() - ledOrigMillis, i);
    r = (byte)(c >> 16) * neoRed   / 255;
    g = (byte)(c >>  8) * neoGreen / 255;
    b = (byte)(c)       * neoBlue  / 255;
    strip.setPixelColor(i, r, g, b);
  }
  strip.show();
}



////////////////////////////////////////////////////////////////////////////////
// HTTP functions
////////////////////////////////////////////////////////////////////////////////

// xxx
void httpProcess(EthernetClient ec, String url) {
  ledOrigMillis = millis();
  httpHeader(ec);
  if(url.startsWith("/led")) {
    url.replace("/led", "");
    if(url.startsWith("/sequence")) {
      url.replace("/sequence/", "");
      ledSequenceNo = url.toInt();
    }
    if(url.startsWith("/speed")) {
      url.replace("/speed/", "");
      ledSpeed = url.toInt();
    }
    if(url.startsWith("/brightness")) {
      url.replace("/brightness/", "");
      ledBrightness = url.toInt();
    }
  }
  httpForm(ec);
  httpFooter(ec);
}

// displays html form for led control
void httpForm(EthernetClient ec) {
  ec.println(F("<form>"));
  ec.println(F("  <select onchange=\"window.location='/led/sequence/' + this.value\">"));
  ec.println(F("    <option value=\"\">sequence</option>"));
  ec.println(F("    <option value=\"0\">0 - blank</option>"));
  ec.println(F("    <option value=\"1\">1 - scan left</option>"));
  ec.println(F("    <option value=\"2\">2 - scan right</option>"));
  ec.println(F("    <option value=\"3\">3 - scan left-right</option>"));
  ec.println(F("    <option value=\"4\">4 - checker</option>"));
  ec.println(F("    <option value=\"10\">10 - fade up</option>"));
  ec.println(F("    <option value=\"11\">11 - fade down</option>"));
  ec.println(F("    <option value=\"12\">12 - chase sequence</option>"));
  ec.println(F("    <option value=\"13\">13 - rgb ball ping-pong</option>"));
  ec.println(F("  </select><br/>"));
  ec.println(F("  <select onchange=\"window.location='/led/speed/' + this.value\">"));
  ec.println(F("    <option value=\"\">speed</option>"));
  ec.println(F("    <option value=\"10\">10</option>"));
  ec.println(F("    <option value=\"20\">20</option>"));
  ec.println(F("    <option value=\"50\">50</option>"));
  ec.println(F("    <option value=\"100\">100</option>"));
  ec.println(F("    <option value=\"200\">200</option>"));
  ec.println(F("    <option value=\"500\">500</option>"));
  ec.println(F("  </select><br/>"));
  ec.println(F("  <select onchange=\"window.location='/led/brightness/' + this.value\">"));
  ec.println(F("    <option value=\"\">brightness</option>"));
  ec.println(F("    <option value=\"1\">1</option>"));
  ec.println(F("    <option value=\"2\">2</option>"));
  ec.println(F("    <option value=\"4\">4</option>"));
  ec.println(F("    <option value=\"8\">8</option>"));
  ec.println(F("    <option value=\"16\">16</option>"));
  ec.println(F("    <option value=\"32\">32</option>"));
  ec.println(F("    <option value=\"64\">64</option>"));
  ec.println(F("    <option value=\"128\">128</option>"));
  ec.println(F("    <option value=\"255\">255</option>"));
  ec.println(F("  </select><br/>"));
  ec.println(F("</form>"));
}

// displays html header fields and start html body
void httpHeader(EthernetClient ec) {
  ec.println(F("HTTP/1.1 200 OK"));
  ec.println(F("Content-Type: text/html"));
  ec.println(F("Connection: close"));
  ec.println();
  ec.println(F("<!DOCTYPE HTML>"));
  ec.println(F("<html>"));
  ec.println(F("<head>"));
  ec.println(F("  <title>LEDControl</title>"));
  ec.println(F("  <style>"));
  ec.println(F("    * {font-face:Arial;}"));
  ec.println(F("    h1 {font-weight:normal;font-size:20px}"));
  ec.println(F("    select {width:150px}"));
  ec.println(F("  </style>"));
  ec.println(F("</head>"));
  ec.println(F("<body>"));
  ec.println(F("<h1>LED Control - "));
  ec.println(LEDCONTROL_VER);
  ec.println(F("</h1>"));
}

// displays html footer and ending
void httpFooter(EthernetClient ec){
  ec.println(F("</body>"));
  ec.println(F("</html>"));
}


////////////////////////////////////////////////////////////////////////////////
// LED sequence algorithms
////////////////////////////////////////////////////////////////////////////////

// returns the actual state of a single led based on selected sequence and millis
uint32_t ledGetSequence(byte seq, long msec, byte led) {
  switch(seq) {
  case  0: 
    return ledGetSequence00(msec, led); 
    break; // monochrome sequences
  case  1: 
    return ledGetSequence01(msec, led); 
    break;
  case  2: 
    return ledGetSequence02(msec, led); 
    break;
  case  3: 
    return ledGetSequence03(msec, led); 
    break;
  case  4: 
    return ledGetSequence04(msec, led); 
    break;
  case 10:                                           // rgb sequences
    return ledGetSequence10(msec, led); 
    break;
  case 11: 
    return ledGetSequence11(msec, led); 
    break;
  case 12: 
    return ledGetSequence12(msec, led); 
    break;
  case 13: 
    return ledGetSequence13(msec, led); 
    break;
  }
}

// blank
uint32_t ledGetSequence00(long msec, byte led) {
  return 0;
}

// scan left
uint32_t ledGetSequence01(long msec, byte led) {
  word mod = (msec / ledSpeed ) % ledCount;
  if(ledCount - mod == led + 1) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
  else return 0;
}

// scan right
uint32_t ledGetSequence02(long msec, byte led) {
  word mod = (msec / ledSpeed ) % ledCount;
  if(mod == led) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
  else return 0;
}

// single ball ping-pong (scan left, then right)
uint32_t ledGetSequence03(long msec, byte led) {
  word mod = (msec / ledSpeed ) % (ledCount * 2 - 2 );
  if(mod < ledCount ) { 
    if(mod == led) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
    else return strip.Color(0, 0, 0);
  } else {
    mod = mod - ledCount + 1;
    if(ledCount - mod == led + 1) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
    else return strip.Color(0, 0, 0);
  }
}

// checker
uint32_t ledGetSequence04(long msec, byte led) {
  byte mod = (msec / ledSpeed ) % 3;
  if(mod == led % 3) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
  else return 0;
}

// fade up
uint32_t ledGetSequence10(long msec, byte led) {
  if(msec > ledSpeed * ledBrightness) return strip.Color(ledBrightness, ledBrightness, ledBrightness);
  else return strip.Color(msec/ledSpeed, msec/ledSpeed, msec/ledSpeed);
}

// fade down
uint32_t ledGetSequence11(long msec, byte led) {
  if(msec > ledSpeed * ledBrightness) return strip.Color(0, 0, 0);
  else return strip.Color(ledBrightness - msec/ledSpeed, ledBrightness - msec/ledSpeed, ledBrightness - msec/ledSpeed);
}

// chase sequence
uint32_t ledGetSequence12(long msec, byte led) {
  uint32_t led1 = ledGetSequence03(msec,              led);
  uint32_t led2 = ledGetSequence03(msec - ledSpeed,   led);
  uint32_t led3 = ledGetSequence03(msec - ledSpeed*2, led);
  uint32_t led4 = ledGetSequence03(msec - ledSpeed*3, led);
  uint32_t led5 = ledGetSequence03(msec - ledSpeed*4, led);
  uint32_t led6 = ledGetSequence03(msec - ledSpeed*5, led);
  uint32_t ret = 0;
  if(led1 > 0) ret = ret + strip.Color(ledBrightness,     ledBrightness,     ledBrightness    );
  if(led2 > 0) ret = ret + strip.Color(ledBrightness / 2, ledBrightness / 2, ledBrightness / 2);
  if(led3 > 0) ret = ret + strip.Color(ledBrightness / 4, ledBrightness / 4, ledBrightness / 4);
  if(led4 > 0) ret = ret + strip.Color(ledBrightness / 8, ledBrightness / 8, ledBrightness / 8);
  return ret;
}

// rgb ball ping-pong
uint32_t ledGetSequence13(long msec, byte led) {
  uint32_t led1 = ledGetSequence03(msec,       led);
  uint32_t led2 = ledGetSequence03(msec * 1.3, led);
  uint32_t led3 = ledGetSequence03(msec * 2.1, led);
  uint32_t led4 = ledGetSequence03(msec * 3.7, led);
  uint32_t ret = 0;
  if(led1 > 0) ret = ret + strip.Color(ledBrightness, 0, 0);
  if(led2 > 0) ret = ret + strip.Color(0, ledBrightness, 0);
  if(led3 > 0) ret = ret + strip.Color(0, 0, ledBrightness);
  if(led4 > 0) ret = ret + strip.Color(ledBrightness/2, ledBrightness/2, 0);
  return ret;
}


